from django.urls import path
from . import views
from .views import UserRegisterView

app_name = 'users'

urlpatterns = [
    path('register', UserRegisterView.as_view(), name='register'),
    path('welcome', views.welcome , name='welcome')
]