from django.test import TestCase, Client

# Create your tests here.
class Test(TestCase):
    def test_url_register_exist(self):
        response = Client().get('/user/register')
        self.assertEquals(response.status_code, 200)

    def test_complete_register(self):
        response = Client().get('/user/register')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Register", html_kembalian)
        self.assertIn("Username", html_kembalian)
        self.assertIn("Password", html_kembalian)

    def test_template_register_exist(self):
        response = Client().get('/user/register')
        self.assertTemplateUsed(response, 'registration/register.html')

    def test_url_login_exist(self):
        response = Client().get('/user/login/')
        self.assertEquals(response.status_code, 200)

    def test_complete_login(self):
        response = Client().get('/user/register')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Login", html_kembalian)
        self.assertIn("Username", html_kembalian)
        self.assertIn("Password", html_kembalian)

    def test_template_login_exist(self):
        response = Client().get('/user/register')
        self.assertTemplateUsed(response, 'registration/register.html')

    def test_url_welcomePage_exist(self):
        response = Client().get('/user/welcome')
        self.assertEquals(response.status_code, 200)

    def test_complete_welcomePage(self):
        response = Client().get('/user/welcome')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Welcome", html_kembalian)

    def test_template_welcomePage_exist(self):
        response = Client().get('/user/welcome')
        self.assertTemplateUsed(response, 'registration/welcome.html')
