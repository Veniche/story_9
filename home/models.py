from django.db import models
from django.urls import reverse

# Create your models here.
class Matkul(models.Model):
    NamaMatkul = models.CharField(max_length=50)
    Dosen = models.CharField(max_length=50)
    sks = models.IntegerField(blank=True, null=True)
    deskripsi = models.CharField(max_length=100)
    smst = models.CharField(max_length=15)
    kelas = models.CharField(max_length=50)

    def get_absolute_url(self):
        return reverse('home:list')

class Kegiatan(models.Model):
    NamaKegiatan = models.CharField(max_length=50)

    def __str__(self):
        return self.NamaKegiatan

    def get_absolute_url(self):
        return reverse('home:kegiatan')

class Peserta(models.Model):
    NamaKegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    Nama = models.CharField(max_length=50)

    def __str__(self):
        return self.Nama

    def get_absolute_url(self):
        return reverse('home:kegiatan')