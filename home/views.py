from django.shortcuts import render
from django.views.generic import ListView, DetailView, DeleteView, CreateView
from django.http import HttpResponse
from .models import Matkul, Kegiatan, Peserta
from django.urls import reverse_lazy
from django.http import JsonResponse
import requests
import json
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

# Create your views here.

def index(request):
    return render(request, 'home/index.html')

def about(request):
    return render(request, 'home/about.html')

def project(request):
    return render(request, 'home/project.html')

def accordion(request):
    return render(request, 'home/accordion.html')

def search(request):
    response = {}
    return render(request, 'home/search.html', response)

def data(request):
    arg = request.GET['q']
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    r = requests.get(url_tujuan)
    data = json.loads(r.content)
    return JsonResponse(data, safe=False)

class MatkulView(ListView):
    model = Matkul
    template_name = 'home/list.html'

class MatkulDetailView(DetailView):
    model = Matkul
    template_name = 'home/detail.html'

@method_decorator(login_required(login_url='login'), name='dispatch')
class MatkulDeleteView(DeleteView):
    model = Matkul
    template_name = 'home/delete.html'
    success_url = reverse_lazy('home:list')

# @login_required(login_url='login')
@method_decorator(login_required(login_url='login'), name='dispatch')
class MatkulCreateView(CreateView):
    model = Matkul
    template_name = "home/matkul_create.html"
    fields = '__all__'

class ListKegiatanView(ListView):

    context_object_name = "list_kegiatan"
    queryset = Kegiatan.objects.all()
    template_name='home/kegiatan.html'

    def get_context_data(self, **kwargs):
        context = super(ListKegiatanView, self).get_context_data(**kwargs)
        context['orang'] = Peserta.objects.all()
        return context

@method_decorator(login_required(login_url='login'), name='dispatch')
class AddKegiatanView(CreateView):
    model = Kegiatan
    template_name = 'home/add_kegiatan.html'
    fields = '__all__'

@method_decorator(login_required(login_url='login'), name='dispatch')
class AddPesertaView(CreateView):
    model = Peserta
    template_name = 'home/add_peserta.html'
    fields = '__all__'